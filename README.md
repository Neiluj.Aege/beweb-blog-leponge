# Blog.L-Eponge

==== EXPLICATION ====

Suite aux cours sur le MVC, création d'un blog suivant un mini modele MvC.

Cette evaluation est réalisée en Php & Mysql. 

Utilisation de la POO & Requêtes en PDO

Utilisation de tinymce pour le CRUD de la page admin.

Aucune modification ou factorisation n'a été effectué depuis la fin de l'evaluation.

Je souhaite pouvoir comparer l'évolution de mon code à travers le temps.

==== INSTALLATION ====

- Vous aurez besoin d'un systeme LAMP (Apache / Mysql / PhP) pour pouvoir faire fonctionner l'application
    - Apache 2.4
    - PhP 7.2
    - MySql 5.7

- Vous devez créer un virtual host à l'addresse : http://www.blog-leponge.com/ afin que tout fonctionne

- Dans le dossier models, ouvrez votre terminal, connectez vous à Mysql, puis faites un source de db_create.sql, afin de créér la base de données. Vous devrez la remplir manuellement afin d'afficher des articles  !!
Modifiez le fichier log_sql.php afin de mettre vos identifiants de connections à la base de données.

Bonne balade !